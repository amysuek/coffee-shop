﻿import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
//import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { RouterModule } from '@angular/router'
import { HttpModule } from '@angular/http'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { AppComponent } from './app.component'
import { NavBarComponent } from './navbar/navbar.component'
import { StoreComponent } from './store/store.component'
import { OrderComponent } from './order/order.component'
import { ContactComponent } from './contact/contact.component'
import { HomeComponent } from './home/home.component'
import { SelectProductComponent } from './common/select-product.component'
import { OrderCompleteComponent } from './order/complete.component'
import { ProductService } from './common/service/product.service'
import { OrderService } from './common/service/order.service'
import { EmptyRowsPipe } from './common/empty-rows.pipe'

import { BsDatepickerModule, CarouselModule, CarouselConfig, ModalModule } from 'ngx-bootstrap'
//import { ModalDirective } from 'ngx-bootstrap/modal'
import { appRoutes } from './routes'


@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        BsDatepickerModule.forRoot(),
        CarouselModule.forRoot(),
        ModalModule.forRoot(),
        RouterModule.forRoot(appRoutes)],
    declarations: [AppComponent,
        NavBarComponent,
        StoreComponent,
        OrderComponent,
        HomeComponent,
        SelectProductComponent,
        OrderCompleteComponent,
        EmptyRowsPipe,
        ContactComponent ],
    providers: [ProductService, OrderService],
    bootstrap: [ AppComponent ]
})
export class AppModule { }