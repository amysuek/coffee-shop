﻿import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { CarouselConfig, CarouselModule } from 'ngx-bootstrap'
import { ProductService } from '../common/service/product.service'
import { IProduct } from '../common/model/product.model'

@Component({
    templateUrl: '/app/store/store.component.html'
})
    
export class StoreComponent implements OnInit {
    products: IProduct[]
    errorMessage : string

    constructor(private router: Router, private _productService: ProductService) {

    }

    ngOnInit() {
        this._productService.getProducts().subscribe(
            products => this.products = products,
            error => this.errorMessage = <any>error)
    }

}
