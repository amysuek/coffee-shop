﻿import { Component, OnInit } from '@angular/core'
import { Router, ActivatedRoute } from '@angular/router'
import { OrderItem } from './../common/model/orderitem.model'
import { ProductService } from './../common/service/product.service';
import { OrderService } from './../common/service/order.service';
import { IProduct } from './../common/model/product.model';


@Component({
    templateUrl: '/app/order/complete.component.html'
})

export class OrderCompleteComponent implements OnInit {
    articles: OrderItem[] = []
    emptyArticle: OrderItem
    products: IProduct[];
    errorMessage: string
    orderTotalAmount: number = 0.00
    id: number;
    deliveryDate: Date;
    firstName: string;
    lastName: string;
    streetAddress: string;
    city: string;
    state: string;
    zip: string;

    constructor(private productService: ProductService,
                private route: ActivatedRoute,
                private orderService: OrderService, private router : Router) { }

    ngOnInit() {
        this.route.params.subscribe(params => {
            console.log("Id = " + params["id"]);
            this.id = params["id"];
            this.orderService.getOrderItems(params["id"]).subscribe(response => {
                this.articles = response;
                this.productService.getProducts().subscribe(response => {
                    this.products = response;
                    this.orderTotalAmount = this.articles[0].OrderTotalAmount;
                    this.deliveryDate = this.articles[0].DeliveryDate;
                    this.firstName = this.articles[0].FirstName;
                    this.lastName = this.articles[0].LastName;
                    this.streetAddress = this.articles[0].StreetAddress;
                    this.state = this.articles[0].State;
                    this.city = this.articles[0].City;
                    this.zip = this.articles[0].ZipCode;
                    for (var i = 0; i < this.articles.length; i++) {
                        this.articles[i].Name = this.products.find(x => x.Id == this.articles[i].ProductId).Name;
                        this.articles[i].UnitPrice = this.products.find(x => x.Id == this.articles[i].ProductId).Price;
                    }
                });
                console.log(response);
            });
        });
    }
}