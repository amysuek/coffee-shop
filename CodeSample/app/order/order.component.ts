﻿import { Component, OnInit, ViewChild } from '@angular/core'
import { Router } from '@angular/router'
import { OrderItem } from './../common/model/orderitem.model'
import { ProductService } from '../common/service/product.service';
import { OrderService } from '../common/service/order.service';
import { IProduct } from './../common/model/product.model';
import { BsDatepickerConfig } from 'ngx-bootstrap/datepicker';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
    templateUrl: '/app/order/order.component.html',
    providers: [OrderService]
})

export class OrderComponent implements OnInit {
    @ViewChild('deliveryDateModal') public deliveryDateModal: ModalDirective;
    articles: OrderItem[] = []
    emptyArticle: OrderItem
    selectedProduct: IProduct
    errorMessage: string
    orderTotalAmount: number = 0.00
    minDate: Date = new Date();
    deliveryDate: Date;
    firstName: string = "";
    lastName: string = "";
    streetAddress: string = "";
    city: string = "";
    state: string = "";
    zipCode: string = "";
    bsConfig: Partial<BsDatepickerConfig> = Object.assign({}, { containerClass: 'theme-default' });

    constructor(private productService: ProductService,
                private orderService: OrderService, private router : Router) { }

    ngOnInit() {
        this.addEmptyRow();
    }

    addEmptyRow() {
        this.emptyArticle = {
            ProductId: 0,
            Name: "",
            UnitPrice: 0,
            Quantity: 0,
            ProductTotalAmount: 0,
            OrderTotalAmount: 0,
            DeliveryDate: new Date(),
            FirstName: "",
            LastName: "",
            StreetAddress: "",
            City: "",
            State: "",
            ZipCode: ""
        }
        this.articles.push(this.emptyArticle);
    }

    removeRow(index: number) {
        this.articles.splice(index, 1);
        this.calculateOrderTotal();
    }

    productChange(productId: any, article: OrderItem) {
        this.productService.getProduct(productId).subscribe(
            data => {
                this.selectedProduct = data;
                article.UnitPrice = this.selectedProduct.Price;
                article.ProductId = productId;
                this.quantityChange(article);
            },
            error => this.errorMessage = <any>error);
    }

    quantityChange(article: OrderItem) {
        article.ProductTotalAmount = article.Quantity * article.UnitPrice;
        this.calculateOrderTotal();
    }

    calculateOrderTotal() {
        this.orderTotalAmount = 0.00
        this.articles.forEach(article => {
            this.orderTotalAmount += article.ProductTotalAmount;
        });
    }

    showDeliveryDateModal() {
        this.deliveryDateModal.show();
    }

    hideDeliveryDateModal() {
        this.deliveryDateModal.hide();
    }

    submit() {
        if (this.deliveryDate != null) {
            for (var i = 0; i < this.articles.length; i++) {
                this.articles[i].DeliveryDate = this.deliveryDate;
                this.articles[i].OrderTotalAmount = this.orderTotalAmount;
                this.articles[i].FirstName = this.firstName;
                this.articles[i].LastName = this.lastName;
                this.articles[i].StreetAddress = this.streetAddress;
                this.articles[i].City = this.city;
                this.articles[i].State = this.state;
                this.articles[i].ZipCode = this.zipCode;
            }
            this.orderService.addOrders(this.articles).subscribe(response => {
                this.router.navigate(['/complete', response.Id])
            });
        } else {
            this.showDeliveryDateModal();
        }
    }
}