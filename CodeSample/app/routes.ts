﻿import { Routes } from '@angular/router'
import { StoreComponent } from './store/store.component'
import { HomeComponent } from './home/home.component'
import { OrderComponent } from './order/order.component'
import { ContactComponent } from './contact/contact.component'
import { OrderCompleteComponent } from './order/complete.component'


export const appRoutes: Routes = [
    { path: 'store', component: StoreComponent },
    { path: 'order', component: OrderComponent },
    { path: 'complete/:id', component: OrderCompleteComponent },
    { path: 'contact', component: ContactComponent },
    { path: 'home', component: HomeComponent },
    { path: '', redirectTo: '/home', pathMatch: 'full' }
]