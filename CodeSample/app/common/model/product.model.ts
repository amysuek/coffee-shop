﻿export interface IProduct {
    Id: number,
    Name: string,
    Price: number,
    Url: string
}