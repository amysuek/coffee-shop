﻿export interface OrderHeader {
    Id: number,
    OrderDate: Date,
    TotalAmount: number,
    DeliveryDate: Date,
    CreatedBy: string,
    CreatedDate: Date
}