﻿export interface OrderItem {
    ProductId: number,
    Name: string,
    UnitPrice: number,
    Quantity: number,
    ProductTotalAmount: number,
    OrderTotalAmount: number,
    DeliveryDate: Date,
    FirstName: string,
    LastName: string,
    StreetAddress: string,
    City: string,
    State: string,
    ZipCode: string
}