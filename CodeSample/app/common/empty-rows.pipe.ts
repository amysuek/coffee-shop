﻿import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'emptyRows' })
export class EmptyRowsPipe implements PipeTransform {
    transform(value: any, size: number) {
        if (!value || !size) {
            size = 10;
        }
        var missing = size - (value ? value.length : 0);
        if (missing < 0) {
            return null;
        }
        return new Array(missing).fill(null);
    }
}