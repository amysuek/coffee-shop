﻿import { Component, OnInit } from '@angular/core'
import { ProductService } from './service/product.service'
import { IProduct } from '../common/model/product.model'

@Component({
    selector: 'select-product',
    template: `<select class="form-control" [(ngModel)]="productValue"> 
                <option [value]="null">Select an option</option>
                <option *ngFor="let product of products" [value]="product.Id" >{{product.Name}}</option>
                </select>`
})

export class SelectProductComponent implements OnInit {
    products: IProduct[];
    errorMessage: string;
    productValue: IProduct = null;

    constructor(private productService: ProductService) { }

    ngOnInit() {
        this.productService.getProducts().subscribe(
            products => this.products = products,
            error => this.errorMessage = <any>error);
    }
}