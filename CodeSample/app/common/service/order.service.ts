﻿import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Rx'
import { OrderItem } from '../model/orderitem.model'
import { OrderHeader } from '../model/orderheader.model'
import { Http, Response, Request, Headers, RequestOptions } from '@angular/http'

@Injectable()
export class OrderService {
    constructor(private http: Http) { }

    addOrders(orders: any[]): Observable<OrderHeader> {
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post("/service/order/", JSON.stringify(orders), options).map((response: Response) => {
            return response.json();
        }).catch(this.handleError);
    }

    getOrderItems(id: number): Observable<OrderItem[]> {

        return this.http.get("/service/order/"+id).map((response: Response) => {
            return <OrderItem>response.json();
        }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}