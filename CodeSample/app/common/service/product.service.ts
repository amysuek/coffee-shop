﻿import { Injectable } from '@angular/core'
import { Observable } from 'rxjs/Rx'
import { IProduct } from '../model/product.model'
import { Http, Response, Request, Headers } from '@angular/http'

@Injectable()
export class ProductService {
    constructor(private http: Http) { }

    getProducts(): Observable<IProduct[]> {
        return this.http.get("/service/product/").map((response: Response) => {
            return <IProduct[]>response.json();
        }).catch(this.handleError);
    }

    getProduct(id: number): Observable<IProduct> {
        return this.http.get("/service/product/" + id).map((response: Response) => {
            return <IProduct>response.json();
        }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}