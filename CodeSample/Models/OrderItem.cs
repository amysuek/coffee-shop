﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web.Mvc;

namespace coffeeshop.Models
{
    public class OrderItem
    {
        public int OrderId { get; set; }

        public string Name { get; set; }

        public decimal OrderTotalAmount { get; set; }

        public int ProductId { get; set; }

        public decimal ProductTotalAmount { get; set; }

        public int Quantity { get; set; }

        public decimal UnitPrice { get; set; }

        public DateTime DeliveryDate { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string StreetAddress { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string ZipCode { get; set; }
    }
}