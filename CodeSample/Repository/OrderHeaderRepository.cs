﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;
using MySql.Data.MySqlClient;
using coffeeshop.Models;
using System.Configuration;

namespace coffeeshop.Repository
{
    public class OrderHeaderRepository
    {
        private string connectionString;
        public OrderHeaderRepository()
        {
            connectionString = ConfigurationManager.ConnectionStrings["websiteDB"].ConnectionString;
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.MySQL);
            }
        }

        public void Add(OrderHeader header)
        {

            using (IDatabase db = Connection)
            {
                db.Insert<OrderHeader>(header);
            }
        }

        public IEnumerable<OrderHeader> GetAll()
        {
            using (IDatabase db = Connection)
            {
                return db.Fetch<OrderHeader>();
            }
        }

        public OrderHeader GetByID(int id)
        {
            using (IDatabase db = Connection)
            {
                return db.SingleById<OrderHeader>(id);
            }
        }

        public void Delete(int id)
        {
            using (IDatabase db = Connection)
            {
                db.Delete<OrderHeader>(id);
            }
        }

        public void Update(OrderHeader header)
        {
            using (IDatabase db = Connection)
            {
                db.Update(header);
            }
        }
    }
}