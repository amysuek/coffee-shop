﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;
using MySql.Data.MySqlClient;
using coffeeshop.Models;
using System.Configuration;

namespace coffeeshop.Repository
{
    public class OrderCustomerRepository
    {
        private string connectionString;
        public OrderCustomerRepository()
        {
            connectionString = ConfigurationManager.ConnectionStrings["websiteDB"].ConnectionString;
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.MySQL);
            }
        }

        public void Add(OrderCustomer customer)
        {

            using (IDatabase db = Connection)
            {
                db.Insert<OrderCustomer>(customer);
            }
        }

        public IEnumerable<OrderCustomer> GetAll()
        {
            using (IDatabase db = Connection)
            {
                return db.Fetch<OrderCustomer>();
            }
        }

        public OrderCustomer GetByID(int id)
        {
            using (IDatabase db = Connection)
            {
                return db.SingleById<OrderCustomer>(id);
            }
        }

        public void Delete(int id)
        {
            using (IDatabase db = Connection)
            {
                db.Delete<OrderCustomer>(id);
            }
        }

        public void Update(OrderCustomer customer)
        {
            using (IDatabase db = Connection)
            {
                db.Update(customer);
            }
        }
    }
}