﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;
using MySql.Data.MySqlClient;
using coffeeshop.Models;
using System.Configuration;

namespace coffeeshop.Repository
{
    public class ProductRepository
    {
        private string connectionString;
        public ProductRepository()
        {
            connectionString = ConfigurationManager.ConnectionStrings["websiteDB"].ConnectionString;
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.MySQL);
            }
        }

        public void Add(Product prod)
        {

            using (IDatabase db = Connection)
            {
                db.Insert<Product>(prod);
            }
        }

        public IEnumerable<Product> GetAll()
        {
            using (IDatabase db = Connection)
            {
                return db.Fetch<Product>();
            }
        }

        public Product GetByID(int id)
        {
            using (IDatabase db = Connection)
            {
                return db.SingleById<Product>(id);
            }
        }

        public void Delete(int id)
        {
            using (IDatabase db = Connection)
            {
                db.Delete<Product>(id);
            }
        }

        public void Update(Product prod)
        {
            using (IDatabase db = Connection)
            {
                db.Update(prod);
            }
        }
    }
}