﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NPoco;
using MySql.Data.MySqlClient;
using coffeeshop.Models;
using System.Configuration;

namespace coffeeshop.Repository
{
    public class OrderDetailRepository
    {
        private string connectionString;
        public OrderDetailRepository()
        {
            connectionString = ConfigurationManager.ConnectionStrings["websiteDB"].ConnectionString;
        }

        public IDatabase Connection
        {
            get
            {
                return new Database(connectionString, DatabaseType.MySQL);
            }
        }

        public void Add(OrderDetail detail)
        {

            using (IDatabase db = Connection)
            {
                db.Insert<OrderDetail>(detail);
            }
        }

        public IEnumerable<OrderDetail> GetAll()
        {
            using (IDatabase db = Connection)
            {
                return db.Fetch<OrderDetail>();
            }
        }

        public OrderDetail GetByID(int id)
        {
            using (IDatabase db = Connection)
            {
                return db.SingleById<OrderDetail>(id);
            }
        }

        public List<OrderDetail> GetByOrderId(int id)
        {
            using (IDatabase db = Connection)
            {
                return db.Fetch<OrderDetail>("SELECT * FROM OrderDetail where orderId = "+id);
            }
        }

        public void Delete(int id)
        {
            using (IDatabase db = Connection)
            {
                db.Delete<OrderDetail>(id);
            }
        }

        public void Update(OrderDetail detail)
        {
            using (IDatabase db = Connection)
            {
                db.Update(detail);
            }
        }
    }
}