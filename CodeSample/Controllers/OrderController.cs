﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using coffeeshop.Models;
using coffeeshop.Services;

namespace coffeeshop.Controllers
{
    [RoutePrefix("service/order")]
    public class OrderController : ApiController
    {
        private OrderHeaderService _orderHeaderService;
        private OrderDetailService _orderDetailService;
        private OrderCustomerService _orderCustomerService;

        protected OrderController()
        {
            _orderHeaderService = new OrderHeaderService();
            _orderDetailService = new OrderDetailService();
            _orderCustomerService = new OrderCustomerService();
        }

        // GET: api/Order/5
        [HttpGet, Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                List<OrderItem> orderItems = new List<OrderItem>();
                OrderHeader orderHeader = _orderHeaderService.getOrderHeader(id);
                OrderCustomer orderCustomer = _orderCustomerService.getOrderCustomer(orderHeader.OrderCustomerId);
                if (orderHeader != null)
                {
                    orderItems = _orderDetailService.getOrderDetails(orderHeader, id);
                    foreach(OrderItem item in orderItems)
                    {
                        item.FirstName = orderCustomer.FirstName;
                        item.LastName = orderCustomer.LastName;
                        item.StreetAddress = orderCustomer.StreetAddress;
                        item.State = orderCustomer.State;
                        item.City = orderCustomer.City;
                        item.ZipCode = orderCustomer.Zip;
                    }
                    return Ok(orderItems);
                }
                else
                {
                    return BadRequest("An error happened in getting the order.");
                }
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        // POST: api/Order/
        [HttpPost, Route("")]
        public IHttpActionResult Add(List<OrderItem> orderItems)
        {
            try
            {
                OrderCustomer orderCustomer = _orderCustomerService.addOrderCustomer(orderItems[0]);
                OrderHeader orderHeader = _orderHeaderService.addOrderHeader(orderItems, orderCustomer.Id);
                if (orderHeader != null)
                {
                    Boolean orderDetailsAdded = _orderDetailService.addOrderDetail(orderItems.ToList(), orderHeader.Id);
                    return Ok(orderHeader);
                }
                else
                {
                    return BadRequest("An error happened in adding the order.");
                }
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }

        }
    }
}
