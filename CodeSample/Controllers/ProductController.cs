﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using coffeeshop.Repository;
using coffeeshop.Models;
using coffeeshop.Services;

namespace coffeeshop.Controllers
{
    [RoutePrefix("service/product")]
    public class ProductController : ApiController
    {
        private ProductService _productService;
        public ProductController()
        {
            _productService = new ProductService();
        }

        [HttpGet,Route("")]
        public IHttpActionResult Get()
        {
            try
            {
                IEnumerable<Product> products = _productService.getAllProducts();
                return Ok(products);
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }
        }

        [HttpGet,Route("{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                Product product = _productService.getProduct(id);
                return Ok(product);
            }
            catch(Exception ex)
            {
                return InternalServerError(ex);
            }
        }
    }
}
