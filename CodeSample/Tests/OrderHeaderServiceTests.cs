﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using coffeeshop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using coffeeshop.Models;

namespace coffeeshop.Services.Tests
{
    [TestClass()]
    public class OrderHeaderServiceTests
    {
        [TestMethod()]
        public void addOrderHeaderTest()
        {
            List<OrderItem> orderItems = new List<OrderItem>();
            OrderItem orderItem = new OrderItem();
            orderItem.ProductId = 1;
            orderItem.ProductTotalAmount = 5;
            orderItem.Quantity = 2;
            orderItem.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderItem.OrderId = 999999;
            orderItem.OrderTotalAmount = (decimal)9.67;
            orderItems.Add(orderItem);
            orderItem.ProductId = 2;
            orderItem.ProductTotalAmount = (decimal)4.67;
            orderItem.Quantity = 1;
            orderItem.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderItem.OrderId = 999999;
            orderItem.OrderTotalAmount = (decimal)9.67;
            orderItems.Add(orderItem);
            OrderHeaderService _orderHeaderService = new OrderHeaderService();
            OrderHeader orderHeader = _orderHeaderService.addOrderHeader(orderItems,4);
            Assert.AreEqual(orderHeader.DeliveryDate, orderItems[0].DeliveryDate);
            Assert.AreEqual(orderHeader.TotalAmount, orderItems[0].OrderTotalAmount);
        }

        [TestMethod()]
        public void getOrderHeaderTest()
        {
            OrderHeaderService _orderHeaderService = new OrderHeaderService();
            OrderHeader orderHeaderReturned = _orderHeaderService.getOrderHeader(1);
            Assert.AreNotEqual(orderHeaderReturned, null);
            Assert.AreEqual(orderHeaderReturned.Id, 1);
        }
    }
}
