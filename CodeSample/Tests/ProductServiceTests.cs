﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using coffeeshop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using coffeeshop.Models;

namespace coffeeshop.Services.Tests
{
    [TestClass()]
    public class ProductServiceTests
    {
        [TestMethod()]
        public void getProductTest()
        {
            ProductService _productService = new ProductService();
            Product product = _productService.getProduct(1);
            Assert.AreNotEqual(product, null);
            Assert.AreEqual(product.Id, 1);
            Assert.AreEqual(product.Name, "Cold Brew");
        }

        [TestMethod()]
        public void getAllProductsTest()
        {
            ProductService _productService = new ProductService();
            IEnumerable<Product> products = _productService.getAllProducts();
            Assert.AreNotEqual(products, null);
            Assert.AreEqual(products.ToList().Count, 2);
        }
    }
}
