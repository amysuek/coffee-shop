﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using coffeeshop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using coffeeshop.Models;

namespace coffeeshop.Services.Tests
{
    [TestClass()]
    public class OrderCustomerServiceTests
    {
        [TestMethod()]
        public void addOrderCustomerTest()
        {
            OrderItem orderItem = new OrderItem();
            orderItem.ProductId = 1;
            orderItem.ProductTotalAmount = 5;
            orderItem.Quantity = 2;
            orderItem.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderItem.OrderId = 999999;
            orderItem.OrderTotalAmount = (decimal)9.67;
            orderItem.FirstName = "Amy";
            orderItem.LastName = "Kamrath";
            orderItem.StreetAddress = "100 Main Street";
            orderItem.City = "Winston Salem";
            orderItem.State = "NC";
            orderItem.ZipCode = "27103";
            OrderCustomerService _orderCustomerService = new OrderCustomerService();
            OrderCustomer orderCustomer = _orderCustomerService.addOrderCustomer(orderItem);
            Assert.AreEqual(orderCustomer.LastName, "Kamrath");
            Assert.AreEqual(orderCustomer.FirstName, "Amy");
        }

        [TestMethod()]
        public void getOrderCustomerTest()
        {
            OrderCustomerService _orderCustomerService = new OrderCustomerService();
            OrderCustomer orderCustomer = _orderCustomerService.getOrderCustomer(1);
            Assert.AreNotEqual(orderCustomer, null);
            Assert.AreEqual(orderCustomer.Id, 1);
        }
    }
}
