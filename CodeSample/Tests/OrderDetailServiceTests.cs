﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using coffeeshop.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using coffeeshop.Models;

namespace coffeeshop.Services.Tests
{
    [TestClass()]
    public class OrderDetailServiceTests
    {
        [TestMethod()]
        public void addOrderDetailTest()
        {
            List<OrderItem> orderItems = new List<OrderItem>();
            OrderItem orderItem = new OrderItem();
            orderItem.ProductId = 1;
            orderItem.ProductTotalAmount = 5;
            orderItem.Quantity = 2;
            orderItem.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderItem.OrderId = 999999;
            orderItem.OrderTotalAmount = (decimal)9.67;
            orderItems.Add(orderItem);
            orderItem.ProductId = 2;
            orderItem.ProductTotalAmount = (decimal)4.67;
            orderItem.Quantity = 1;
            orderItem.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderItem.OrderId = 999999;
            orderItem.OrderTotalAmount = (decimal)9.67;
            orderItems.Add(orderItem);
            OrderDetailService _orderDetailService = new OrderDetailService();
            Boolean orderAdded = _orderDetailService.addOrderDetail(orderItems, 999999);
            Assert.AreEqual(true, orderAdded);
        }

        [TestMethod()]
        public void getOrderDetailTest()
        {
            List<OrderItem> orderItems = new List<OrderItem>();
            OrderItem orderItem = new OrderItem();
            orderItem.ProductId = 1;
            orderItem.ProductTotalAmount = 5;
            orderItem.Quantity = 2;
            orderItem.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderItem.OrderId = 999999;
            orderItem.OrderTotalAmount = (decimal)9.67;
            orderItems.Add(orderItem);
            orderItem.ProductId = 2;
            orderItem.ProductTotalAmount = (decimal)4.67;
            orderItem.Quantity = 1;
            orderItem.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderItem.OrderId = 999999;
            orderItem.OrderTotalAmount = (decimal)9.67;
            orderItems.Add(orderItem);
            OrderHeader orderHeader = new OrderHeader();
            orderHeader.DeliveryDate = Convert.ToDateTime("2017-12-30");
            orderHeader.TotalAmount = (decimal)9.67;
            OrderDetailService _orderDetailService = new OrderDetailService();
            List<OrderItem> orderItemsReturned = _orderDetailService.getOrderDetails(orderHeader, 999999);
            Assert.AreEqual(orderItems.Count, orderItemsReturned.Count);
        }
    }
}
