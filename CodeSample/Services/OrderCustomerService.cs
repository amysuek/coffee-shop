﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using coffeeshop.Repository;
using coffeeshop.Models;

namespace coffeeshop.Services
{
    public class OrderCustomerService
    {
        private OrderCustomerRepository _orderCustomerRepo;

        public OrderCustomerService()
        {
            _orderCustomerRepo = new OrderCustomerRepository();
        }

        public OrderCustomer addOrderCustomer(OrderItem orderItem)
        {
            OrderCustomer orderCustomer = new OrderCustomer();
            orderCustomer.FirstName = orderItem.FirstName;
            orderCustomer.LastName = orderItem.LastName;
            orderCustomer.StreetAddress = orderItem.StreetAddress;
            orderCustomer.City = orderItem.City;
            orderCustomer.State = orderItem.State;
            orderCustomer.Zip = orderItem.ZipCode;
            _orderCustomerRepo.Add(orderCustomer);
            return orderCustomer;
        }

        public OrderCustomer getOrderCustomer(int orderCustomerId)
        {
            OrderCustomer orderCustomer = _orderCustomerRepo.GetByID(orderCustomerId);
            return orderCustomer;
        }
    }
}