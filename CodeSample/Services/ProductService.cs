﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using coffeeshop.Repository;
using coffeeshop.Models;

namespace coffeeshop.Services
{
    public class ProductService
    {
        ProductRepository _productRepo;

        public ProductService()
        {
            _productRepo = new ProductRepository();
        }

        public Product getProduct(int id)
        {
            Product product = _productRepo.GetByID(id);

            return product;
        }

        public IEnumerable<Product> getAllProducts()
        {
            IEnumerable<Product> products = _productRepo.GetAll();

            return products;
        }
    }
}