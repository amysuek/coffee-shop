﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using coffeeshop.Repository;
using coffeeshop.Models;

namespace coffeeshop.Services
{
    public class OrderDetailService
    {
        private OrderDetailRepository _orderDetailRepo;

        public OrderDetailService()
        {
            _orderDetailRepo = new OrderDetailRepository();
        }

        public Boolean addOrderDetail(List<OrderItem> orderItems, int orderHeaderId)
        {
            Boolean orderDetailAdded = false;
            orderItems.ToList().ForEach(orderItem =>
            {
                OrderDetail orderDetail = new OrderDetail();
                orderDetail.ProductId = orderItem.ProductId;
                orderDetail.OrderId = orderHeaderId;
                orderDetail.ProductTotal = orderItem.ProductTotalAmount;
                orderDetail.Quantity = orderItem.Quantity;
                _orderDetailRepo.Add(orderDetail);
                orderDetailAdded = true;
            });
            return orderDetailAdded;
        }

        public List<OrderItem> getOrderDetails(OrderHeader orderHeader,int id)
        {
            List<OrderItem> orderItems = new List<OrderItem>();
            List<OrderDetail> orderDetails = _orderDetailRepo.GetByOrderId(id);
            foreach (OrderDetail orderDetail in orderDetails)
            {
                OrderItem item = new OrderItem();
                item.ProductId = orderDetail.ProductId;
                item.ProductTotalAmount = orderDetail.ProductTotal;
                item.Quantity = orderDetail.Quantity;
                item.OrderTotalAmount = orderHeader.TotalAmount;
                item.DeliveryDate = orderHeader.DeliveryDate;
                item.OrderId = orderDetail.OrderId;
                orderItems.Add(item);
            }
            return orderItems;
        }
    }
}