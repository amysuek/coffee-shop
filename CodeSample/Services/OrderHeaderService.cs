﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using coffeeshop.Repository;
using coffeeshop.Models;

namespace coffeeshop.Services
{
    public class OrderHeaderService
    {
        private OrderHeaderRepository _orderHeaderRepo;

        public OrderHeaderService()
        {
            _orderHeaderRepo = new OrderHeaderRepository();
        }
        
        public OrderHeader addOrderHeader(List<OrderItem> orderItems, int orderCustomerId)
        {
            OrderHeader orderHeader = new OrderHeader();
            orderHeader.TotalAmount = orderItems.ToList()[0].OrderTotalAmount;
            orderHeader.OrderDate = DateTime.Now;
            orderHeader.DeliveryDate = orderItems.ToList()[0].DeliveryDate;
            orderHeader.OrderCustomerId = orderCustomerId;
            _orderHeaderRepo.Add(orderHeader);
            return orderHeader;
        }

        public OrderHeader getOrderHeader(int id)
        {
            OrderHeader orderHeader = _orderHeaderRepo.GetByID(id);
            return orderHeader;
        }
    }
}